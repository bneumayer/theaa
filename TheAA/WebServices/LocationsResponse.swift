//
//  Locations.swift
//  TheAA
//
//  Created by Bratt Neumayer on 6/3/18.
//  Copyright © 2018 Bratt Neumayer. All rights reserved.
//

import Foundation

class LocationsResponse: Codable{
    
    var Locations: Location?
}


class Location: Codable{
    
    var Location: [LocationItem]?
}

class LocationItem: Codable{
    
    var elevation: String?
    var id: String?
    var latitude: String?
    var longitude: String?
    var name: String?
    var region: String?
    var unitaryAuthArea: String?
}



import Foundation
struct Rep : Codable {
	let d : String?
	let gn : Int?
	let hn : Int?
	let pPd : Int?
	let s : Int?
	let v : String?
	let dm : Int?
	let fDm : Int?
	let w : Int?
	let u : Int?
    let `$` : String?

	enum CodingKeys: String, CodingKey {

		case d = "D"
		case gn = "Gn"
		case hn = "Hn"
		case pPd = "PPd"
		case s = "S"
		case v = "V"
		case dm = "Dm"
		case fDm = "FDm"
		case w = "W"
		case u = "U"
        case `$` = "$"
	}
}

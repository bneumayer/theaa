

import Foundation
struct DV : Codable {
	let dataDate : String?
	let type : String?
	let location : Location?

	enum CodingKeys: String, CodingKey {

		case dataDate = "dataDate"
		case type = "type"
		case location
	}
}

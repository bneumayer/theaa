

import Foundation
struct Period : Codable {
	let type : String?
	let value : String?
	let rep : [Rep]?

	enum CodingKeys: String, CodingKey {

		case type = "type"
		case value = "value"
		case rep = "Rep"
	}
}

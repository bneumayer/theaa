

import Foundation
struct DetailLocation : Codable {
	let i : Int?
	let lat : Double?
	let lon : Double?
	let name : String?
	let country : String?
	let continent : String?
	let elevation : Double?
	let period : [Period]?

	enum CodingKeys: String, CodingKey {

		case i = "i"
		case lat = "lat"
		case lon = "lon"
		case name = "name"
		case country = "country"
		case continent = "continent"
		case elevation = "elevation"
		case period = "Period"
	}


}

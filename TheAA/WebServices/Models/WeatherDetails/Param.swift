

import Foundation
struct Param : Codable {
	let name : String?
	let units : String?
    let `$` : String?

	enum CodingKeys: String, CodingKey {

		case name = "name"
		case units = "units"
        case `$` = "$"
	}
}

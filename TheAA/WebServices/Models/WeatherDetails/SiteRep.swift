

import Foundation
struct SiteRep : Codable {
	let wx : Wx?
	let dV : DV?

	enum CodingKeys: String, CodingKey {

		case wx
		case dV
	}
}

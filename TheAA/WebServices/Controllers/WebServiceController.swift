//
//  WebServiceController.swift
//  TheAA
//
//  Created by Bratt Neumayer on 6/3/18.
//  Copyright © 2018 Bratt Neumayer. All rights reserved.
//

import Foundation
import UIKit

class WebServiceController{
    
    let session = URLSession(configuration: .default)
    
    var baseURLString = "http://datapoint.metoffice.gov.uk/public/data/val/wxfcs/all/json/"
    
    let apiKeyQuery = "key=ff9e4d82-6d93-4aab-a192-b99b886d0d1c"
    
    var queryString: String = ""
    
    var urlWithQuery: URL?{
        
        if var urlComponents = URLComponents(string: self.baseURLString) {
            
            urlComponents.query = self.queryString
            return urlComponents.url ?? nil
            
        }
        
        return nil
    }
    
    
    
    init() {
        
    }
    
    
    
}

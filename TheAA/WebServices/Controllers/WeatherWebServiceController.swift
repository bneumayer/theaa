//
//  WeatherWebServiceController.swift
//  TheAA
//
//  Created by Bratt Neumayer on 6/3/18.
//  Copyright © 2018 Bratt Neumayer. All rights reserved.
//

import Foundation
import UIKit
import MapKit

class WeatherWebServiceController: WebServiceController{
    
    private var viewControllerDelegate: MapViewController?
    
    private var existingRegions = [String]()
    
    private var annotations = [WeatherStationAnnotation]()
    
    var dataController : DataController?
    
    
    private(set) var theDataTask: URLSessionDataTask?{
    
        get{
        
            self.prepareSessionURL()
            
            guard let tempUrl = self.urlWithQuery else {return nil}
            
            let dataTask = session.dataTask(with: tempUrl){ data,response,error in
                
                guard let sessionData = data else {
                    
                    self.updateDelegateView()
                    return
                }
                
                self.extractWeatherStations(from: sessionData)
                
//                self.finishUp()
            }
            
            return dataTask
        }
        
        set{
            
        }
    }
    
    
    
    init(withDelegate delegate: MapViewController) {
        
        self.viewControllerDelegate = delegate
        
    }
    
    
    
    func prepareSessionURL(){
        
        self.baseURLString += "sitelist"
        self.queryString = apiKeyQuery
        
        print(self.baseURLString,self.queryString)
    }
    
    
    
    func extractWeatherStations(from sessionData: Data){
        
        let decoder = JSONDecoder()

        let weatherStations = try? decoder.decode(LocationsResponse.self, from: sessionData)
            
        guard let locations = weatherStations?.Locations?.Location else {return}
        
        
        for location in locations{
            
            let _ = dataController?.createWeatherStation(location: location)
        }
    }
    
    
    
    func updateDelegateView(){
        
        DispatchQueue.main.async {
            self.viewControllerDelegate?.updateView()
        }
    }
}

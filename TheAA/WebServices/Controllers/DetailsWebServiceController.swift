//
//  DetailsWebServiceController.swift
//  TheAA
//
//  Created by Bratt Neumayer on 6/4/18.
//  Copyright © 2018 Bratt Neumayer. All rights reserved.
//

import Foundation
import UIKit
import MapKit

class DetailsWebServiceController: WebServiceController{
    
    private var viewControllerDelegate: DetailsViewController?
    
    var dataController : DataController?
    
    var locationID: String
    
    
    private(set) var theDataTask: URLSessionDataTask?{
        
        get{
            
            self.prepareSessionURL()
            
            guard let tempUrl = self.urlWithQuery else {return nil}
            
            let dataTask = session.dataTask(with: tempUrl){ data,response,error in
                
                guard let sessionData = data else {
                    
                    self.updateDelegateView()
                    return
                }
                
                self.extract(sessionData)
                
            }
            
            return dataTask
        }
        
        set{
            
        }
    }
    
    
    
    init(withDelegate delegate: DetailsViewController) {
        
        self.viewControllerDelegate = delegate
        
        self.locationID = delegate.weatherStation!.id!//it's not the cleanest, but we've made sure this value is available in earlier stages, still, should be done elsehow to be honest
    }
    
    
    
    func prepareSessionURL(){
        
        self.baseURLString += self.locationID
        self.queryString = "res=daily&\(apiKeyQuery)"
        
        print(self.baseURLString,self.queryString)
    }
    
    
    
    func extract(_ sessionData: Data){
        
        print(String(data: sessionData, encoding:.utf8))
        
        let decoder = JSONDecoder()

        let details = try? decoder.decode(DetailsResponse.self, from: sessionData)
        

        // I auto-generated the Details-Models from the json response.
        
        // I would now persist the data into the CoreData
        
        // Same procedure as I did with the weather-stations/location.
        
        // It's just time consuming to find out what is what (response-JSON) and to build the CoreData models :)
        
        // I believe the important thing is to show that once the data is there, it's simple to continue (still time consuming to write the code)
        
        
    }
    
    
    
    func updateDelegateView(){
        
        DispatchQueue.main.async {
            
            
        }
    }
}

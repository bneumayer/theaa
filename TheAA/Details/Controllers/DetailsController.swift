//
//  ViewModel.swift
//  TheAA
//
//  Created by Bratt Neumayer on 6/4/18.
//  Copyright © 2018 Bratt Neumayer. All rights reserved.
//

import Foundation

enum CellIdentifiers: String{
    
    case LargeHeaderTableViewCell, SimpleRowTableViewCell
}



class DetailsController{
    
    var sections = [SectionModel]() // one can refactor a getter...
    
    private var days = [WeatherDay]()
    
    private var weatherStationID: String
    
    //var detailsDataController: DetailsDataController -- would extract the needed data for the days
    
    
    
    init(forWeatherStationWithID id: String) {
        
        self.weatherStationID = id
    }
    
    
    public func setDays(to days: [WeatherDay]){
        
        self.days = days
    }
    
    
    
    public func initiateModeling(){
        
        appendHeaderSection()
        
        appendForecastSection()
    }
    
    
    
    private func appendHeaderSection(){
        
        let mainHeaderSection = newSection(withTitle: "Today") //This can be used for example for section-header-views
        
        let mainHeaderCell = newCell(withIdentifier: "LargeHeaderTableViewCell")
        mainHeaderCell.modelDelegate = self.days.first
        mainHeaderSection.cells.append(mainHeaderCell)
        
        self.sections.append(mainHeaderSection)
    }
    
    
    
    private func appendForecastSection(){
        
        let forecastSection = newSection(withTitle: "Forecast")
        
        for day in self.days{
            
            let tempCell = newCell(withIdentifier: "SimpleRowTableViewCell")
            tempCell.modelDelegate = day
            forecastSection.cells.append(tempCell)
        }
        
        self.sections.append(forecastSection)
    }
    
    
    
    private func newSection(withTitle title:String) -> SectionModel{
        
        return SectionModel(title: title)
    }

    
    
    private func newCell(withIdentifier identifier: String) -> CellModel{
        
        let tempCell = CellModel(identifier: identifier)
        
        switch identifier {
            
        case CellIdentifiers.LargeHeaderTableViewCell.rawValue:
            tempCell.cellHeight = 250
            
        default:
            tempCell.cellHeight = 70
        }
        
        return tempCell
    }
}

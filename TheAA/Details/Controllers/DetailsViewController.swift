//
//  DetailsViewController.swift
//  TheAA
//
//  Created by Bratt Neumayer on 6/3/18.
//  Copyright © 2018 Bratt Neumayer. All rights reserved.
//

import UIKit

class DetailsViewController: UIViewController {

    @IBOutlet weak var detailsTableView: UITableView!
    
    var weatherStation: WeatherStation?
    
    var detailsWebServiceController: DetailsWebServiceController?
    
    var detailsController: DetailsController?
    
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.prepareTableView()
        
        self.detailsController = DetailsController(forWeatherStationWithID: self.weatherStation!.id!)
        
        self.title = self.weatherStation?.title ?? "-"
        
        self.detailsWebServiceController = DetailsWebServiceController(withDelegate: self)
        
        self.detailsWebServiceController?.theDataTask?.resume()
        
        // If one runs the app and looks at the output, one will see that the correct data is being downloaded for the corresponding weather station.
        
        // Say we use now an NSFetchedResultsControllerDelegate (e.g)
        
        // Once data is saved, we'd call:
        
        // self.detailsController?.setDays(to: <#T##[WeatherDay]#>)
        
        // Then we call
        
        self.detailsController!.initiateModeling()
        
    }
    
    
    
    func prepareTableView(){
     
        self.detailsTableView.delegate = self
        self.detailsTableView.dataSource = self
        
        let headerCellNib = UINib(nibName: "LargeHeaderTableViewCell", bundle: nil)
        let dayCellNib = UINib(nibName: "SimpleRowTableViewCell", bundle: nil)
        
        self.detailsTableView.register(headerCellNib, forCellReuseIdentifier: "LargeHeaderTableViewCell")
        self.detailsTableView.register(dayCellNib, forCellReuseIdentifier: "SimpleRowTableViewCell")
    }

    
    
    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
    }
    

}


extension DetailsViewController: UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return self.detailsController!.sections[indexPath.section].cells[indexPath.row].cellHeight!
    }
    
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if let tempCell = cell as? BaseCell{
            
            tempCell.bind(with: self.detailsController!.sections[indexPath.section].cells[indexPath.row].modelDelegate)
        }
    }
}



extension DetailsViewController: UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.detailsController!.sections[section].cells.count
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let identifier = self.detailsController?.sections[indexPath.section].cells[indexPath.row].identifier else  {
            
            return UITableViewCell()
        }
        
        return tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath)
    }
}

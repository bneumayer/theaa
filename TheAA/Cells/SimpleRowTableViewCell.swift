//
//  SimpleRowTableViewCell.swift
//  TheAA
//
//  Created by Bratt Neumayer on 6/3/18.
//  Copyright © 2018 Bratt Neumayer. All rights reserved.
//

import UIKit

class SimpleRowTableViewCell: UITableViewCell, BaseCell {
    
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var significantLabel: UILabel!
    @IBOutlet weak var precipitationLabel: UILabel!
    @IBOutlet weak var windSpeedLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    
    func bind(with model: Any?) {
        
        DispatchQueue.main.async {
            
            guard let day = model as? WeatherDay else {return}
            
            self.dayLabel.text = day.dayName ?? ""
            self.significantLabel.text = day.significant ?? ""
            self.precipitationLabel.text = "\(day.precipitation)"
            self.windSpeedLabel.text = "\(day.windSpeed)"
            self.temperatureLabel.text = "\(day.temperature)"
        }
    }
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

//
//  SectionModel.swift
//  TheAA
//
//  Created by Bratt Neumayer on 6/3/18.
//  Copyright © 2018 Bratt Neumayer. All rights reserved.
//

import Foundation


protocol SectionModelProtocol {
    var title: String? {get set}
    var cells: [CellModel] {get set}
    
    init(title:String)
}


class SectionModel:SectionModelProtocol{
    
    var title: String?
    var cells = [CellModel]()
    
    required init(title: String) {
        self.title = title
    }
    
    
}

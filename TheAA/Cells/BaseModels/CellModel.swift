//
//  CellModel.swift
//  TheAA
//
//  Created by Bratt Neumayer on 6/3/18.
//  Copyright © 2018 Bratt Neumayer. All rights reserved.
//

import Foundation
import UIKit


protocol BaseCell {
    
    func bind(with model:Any?)
}

protocol CellModelProtocl {
    
    var controllingView: UIViewController? {get set}
    
    var identifier: String {get set}
    
    var modelDelegate: Any? {get set}
    
    var cellHeight: CGFloat? {get set}
    
    init(identifier: String)
}


class CellModel: CellModelProtocl{
    
    var controllingView: UIViewController?
    
    var identifier: String
    
    var modelDelegate: Any?
    
    var cellHeight: CGFloat?
    
    required init(identifier: String) {
        
        self.identifier  = identifier
    }
}

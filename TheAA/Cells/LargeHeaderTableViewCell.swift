//
//  LargeHeaderTableViewCell.swift
//  TheAA
//
//  Created by Bratt Neumayer on 6/3/18.
//  Copyright © 2018 Bratt Neumayer. All rights reserved.
//

import UIKit

class LargeHeaderTableViewCell: UITableViewCell, BaseCell {
    
    
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var significantLabel: UILabel!
    @IBOutlet weak var precipitationLabel: UILabel!
    @IBOutlet weak var windSpeedLabel: UILabel!
    
    
    
    func bind(with model: Any?) {
        
        guard let weatherStation = model as? WeatherStation else { return }
        guard let days = weatherStation.days as? [WeatherDay] else {return}
        let currentDay = days[0]
        
        DispatchQueue.main.async {
            
            self.temperatureLabel.text = "\(currentDay.temperature)"
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
}

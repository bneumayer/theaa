//
//  DataController.swift
//  TheAA
//
//  Created by Bratt Neumayer on 6/3/18.
//  Copyright © 2018 Bratt Neumayer. All rights reserved.
//

import Foundation
import CoreData
import MapKit



extension DataController: NSFetchedResultsControllerDelegate{
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        switch type {
        
        case .insert:
            
            if let object = anObject as? WeatherStation{
                
                let coordinates = CLLocationCoordinate2D(latitude: object.latitude, longitude: object.longitude)
                
                guard let id = object.id, let title = object.title, let subtitle = object.subtitle else {break} //shouldn't need to, but for some reason it's crushing..
                
                let annotation = WeatherStationAnnotation(id: id, title: title, subtitle: subtitle, coordinate: coordinates)
                
                self.viewControllerDelegate?.insert(annotation: annotation)
            }
            
        default:
            break
        }
    }
}

class DataController: NSObject{
    
    private let persistentContainer: NSPersistentContainer = NSPersistentContainer(name: "TheAA")
    
    var viewControllerDelegate: MapViewController?
    
    
    
    init(delegate: MapViewController) {
        
        self.viewControllerDelegate = delegate
    }
    
    
    
    
    func loadPersistentStores(){
        
        persistentContainer.loadPersistentStores { (persistentStoreDescription, error) in
            
            if let error = error {
                
                print(error.localizedDescription)
                
            } else {
                
                do {
                    
                    try self.fetchController.performFetch()
                    
                } catch {
                    
                    print(error.localizedDescription)
                }
            }
        }
    }
    
    
    
    func removeAll(){
        
        guard self.fetchController.fetchedObjects != nil else {return}
        
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: self.fetchController.fetchRequest as! NSFetchRequest<NSFetchRequestResult>)
        
        do{
            
            try self.persistentContainer.viewContext.execute(deleteRequest)
            try self.persistentContainer.viewContext.save()
            
        }catch{
            print("removeAll",error.localizedDescription)
        }
        
        self.viewControllerDelegate?.updateView()
        
    }
    
    
    
    func refreshFetch(){
        
        try? self.fetchController.performFetch()
    }
    
    
    var specificID: String?
    
    lazy var fetchController: NSFetchedResultsController<WeatherStation> = {
        
        let fetchRequest: NSFetchRequest<WeatherStation> = WeatherStation.fetchRequest()
        
        if specificID != nil{
            
            fetchRequest.predicate = NSPredicate(format: "id == %@", specificID!)
        }
        
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "id", ascending: false)]
        
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: self.persistentContainer.viewContext, sectionNameKeyPath: nil, cacheName: nil)
        
        fetchedResultsController.delegate = self
        
        return fetchedResultsController
    }()
    
    
    
    private var existingRegions = [String]()
    func createWeatherStation(location: LocationItem ){
        
        guard let region = location.region else {return}
        guard existingRegions.index(of: region) == nil else {return}
        
        guard let latitudeString = location.latitude else {return}
        guard let latitude = Double(latitudeString) else {return}
        
        guard let longitudeString = location.longitude else {return}
        guard let longitude = Double(longitudeString) else {return}
        
        guard let name = location.name else {return}
        
        guard let id = location.id else {return}
        
        let weatherStation = WeatherStation(context: self.persistentContainer.viewContext)
        
        weatherStation.id = id
        weatherStation.latitude = latitude
        weatherStation.longitude = longitude
        weatherStation.title = name
        weatherStation.subtitle = region
        weatherStation.lastUpdated = Date()
        
        do {
            
            try self.persistentContainer.viewContext.save()
            existingRegions.append(region)
            return
            
        } catch {
            
            self.persistentContainer.viewContext.delete(weatherStation)
        }
    }
    
    
    
    func weatherStation(withID id: String) -> WeatherStation?{
        
        var returnValue: WeatherStation?
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "WeatherStation")
        
        fetchRequest.predicate = NSPredicate(format: "id = %@", id)
        
        do {
            
            if let weatherStation = try self.persistentContainer.viewContext.fetch(fetchRequest).first as? WeatherStation{
                
                returnValue = weatherStation
            }
            
        } catch  {
            
            print(error.localizedDescription)
            
        }
        
        return returnValue
    }
    
    
}

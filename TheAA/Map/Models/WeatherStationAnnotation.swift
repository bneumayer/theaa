//
//  WeatherStationAnnotation.swift
//  TheAA
//
//  Created by Bratt Neumayer on 6/3/18.
//  Copyright © 2018 Bratt Neumayer. All rights reserved.
//

import Foundation

import MapKit

import MapKit

class WeatherStationAnnotation: NSObject, MKAnnotation {
    
    let title: String?
    let subtitle: String?
    let id: String?
    let coordinate: CLLocationCoordinate2D
    
    init(id: String, title: String, subtitle: String, coordinate: CLLocationCoordinate2D) {
        
        self.id = id
        self.title = title
        self.subtitle = subtitle
        self.coordinate = coordinate
        
        super.init()
    }
    
}

//
//  ViewController.swift
//  TheAA
//
//  Created by Bratt Neumayer on 6/3/18.
//  Copyright © 2018 Bratt Neumayer. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class MapViewController: UIViewController {

    
    @IBOutlet weak var weatherMapView: MKMapView!
    
    let regionRadius: CLLocationDistance = 800000
    
    var weatherStationsAnnotations = [WeatherStationAnnotation]()
    
    var initialLocation: CLLocation{
        
        return CLLocation(latitude: 54.80758, longitude: -3.94949)//Dundrennan
    }
    
    var mapRegion: MKCoordinateRegion!{
        
        didSet{
            
            self.weatherMapView.setRegion(mapRegion, animated: true)
        }
    }
    
    
    var dataController: DataController?
    
    
    //
    // MARK:
    //
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        dataController = DataController(delegate: self)
        
        dataController?.loadPersistentStores()
        
        self.weatherMapView.delegate = self
        
        self.mapRegion = MKCoordinateRegionMakeWithDistance(self.initialLocation.coordinate, regionRadius, regionRadius)
        
        self.checkForCachedDataAndIfDoesNotExistThenPleaseDownloadNewWeatherStations_ThankYou_ThisIsJustForFun()
    }
    
    
    //Here we can also check if data was saved for 24 hrs and decide if to refresh or not - didn't add that logic as it's mostly boilerplate.
    func checkForCachedDataAndIfDoesNotExistThenPleaseDownloadNewWeatherStations_ThankYou_ThisIsJustForFun(){
        
        guard let weatherStations = dataController?.fetchController.fetchedObjects else {return}
        
        if weatherStations.count > 0{
            
            for station in weatherStations{
                
                let coordinates = CLLocationCoordinate2D(latitude: station.latitude, longitude: station.longitude)
                let annotation = WeatherStationAnnotation(id: station.id ?? "", title: station.title ?? "", subtitle: station.subtitle ?? "", coordinate: coordinates)
                
                self.weatherMapView.addAnnotation(annotation)
            }
            
        }else{
            
            self.retrieveWeatherStations()
        }
    }
    
    
    
    func retrieveWeatherStations(){
        
        let weatherWebService = WeatherWebServiceController(withDelegate: self)
        weatherWebService.dataController = self.dataController!
        weatherWebService.theDataTask?.resume()
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    
    func updateView(){
        
        self.weatherMapView.removeAnnotations(self.weatherMapView.annotations)
        
        DispatchQueue.main.async {
            self.weatherMapView.addAnnotations(self.weatherStationsAnnotations)
        }
    }
    
    
    
    func setAnnotations(to annotations: [WeatherStationAnnotation]){
        
        print("ASFDQWER")
        self.weatherStationsAnnotations = annotations
    }
    
    
    
    func insert(annotation: MKAnnotation){
        
        DispatchQueue.main.async {
            self.weatherMapView.addAnnotation(annotation)
        }
    }

}

//
// MARK:
//

extension MapViewController: MKMapViewDelegate{
    
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        guard let annotation = annotation as? WeatherStationAnnotation else { return nil }
        
        let identifier = "marker"
        var view: MKMarkerAnnotationView
        
        view = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: identifier)
        view.canShowCallout = true
        view.calloutOffset = CGPoint(x: -5, y: 5)
        view.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
        
        return view
    }
    
    
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        
        guard let annotation = view.annotation as? WeatherStationAnnotation else { return }
        guard let id = annotation.id else {return}//handle this of course
        
        self.dataController?.specificID = id
        self.dataController?.refreshFetch()
        
        if let weatherStation = self.dataController?.fetchController.fetchedObjects?.first{
            
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "DetailsViewController") as! DetailsViewController
            vc.weatherStation = weatherStation
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}





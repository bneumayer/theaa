//
//  MapTets.swift
//  TheAA
//
//  Created by Bratt Neumayer on 6/6/18.
//  Copyright © 2018 Bratt Neumayer. All rights reserved.
//

import XCTest

class MapTets: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    
    
    func testViewDidLoadShouldSetMapViewDelegate(){
        
    }
    
    
    func testViewDidLoadShouldSetMapInitialRegion(){
        
    }
    
    
    func testViewDidLoadShouldCehckForCachedData(){
        
    }
    
    
    func testRetrievedWeatherStationsAreNotEmpty(){
        
    }
    
    
    func testMapAnnotationsAreNotNilWhenUpdatingView(){
        
    }
    
    func testSetAnnotationIsNotNil(){
        
    }
    
    
    func testInsertAnnotation(){
        
    }
    
    
    // And more -> Timewise I do not have time to write/complete the test, I would, but I really don't have more time at the moment.
    // Testing also need additional configurations for CoreData testing/compiling.
    
}
